# Introduction

Le site du prestataire s'occupant du déploiement de la fibre en Isère (Isère Fibre / THD38) publie *régulièrement* une mise à jour des logements qui sont ou seront prochainement éligibles à la fibre. 

Pour tester si une adresse est éligible, il faut sélectionner une série de listes déroulantes qui s'actualisent au fur et à mesure : d'abord on sélectionne une ville, puis le type de rue, puis le nom de la rue et enfin le numéro de la rue. 

Les options pour chaque champ sont actualisés en fonction de la sélection des précédentes options et du contenu de leur base de données. Toutes les adresses ne figurent pas dans leur base à ce jour. Une adresse n'est présente que si la date d'éligibilité (future ou passée) a été fixée.

Ce script a pour but de récupérer toutes les valeurs de la base de données de THD38 pour suivre l'évolution du déploiement de la fibre au cours du temps.

# Fonctionnement général

Le script consiste en une succession de requêtes HTTP POST pour obtenir la liste de toutes les adresses d'une ville et la date d'éligibilité associée. On sélectionne d'abord 1 ville, puis pour chaque type de voie, chaque nom de voie et chaque numéro de voie, on récupère la valeur.

On tape systématiquement le même endpoint (http://iserefibre.fr/te/query.php) mais en donnant un payload différent en fonction du niveau de la recherche (attention à la casse, il n'y a pas d'erreur dans cette documentation. C'est bien `down` pour le premier champ et `Down` pour les suivants) :
- `dropdownValueCommune`: Renvoie la liste des types de voie de la ville
- `dropDownTypeVoie`: Renvoie la liste des noms de voie de ce type dans cette ville
- `dropDownNomVoie`: Renvoie la liste des numéros de cette voie dans cette ville
- `dropDownNumVoie`: Renvoie la date d'éligibilité de cette adresse

La valeur attendue dépend de la profondeur de la recherche, mais elle prend systématiquement la forme suivante : `VILLE|TYPE|NOM|NUMERO`

Exemple: si je veux connaitre les noms des voies de type RUE dans la ville de BRESSON, j'enverrai le payload `dropDownTypeVoie=BRESSON|RUE`

La réponse correpond à la liste des options du sélecteur, formatée en HTML. Il suffit de récupérer le texte contenu dans chaque balise `<option></option>`.