import requests
from functools import reduce
from bs4 import BeautifulSoup

url = "http://www.iserefibre.fr/te/query.php"
city = 'VOREPPE'

def get_streets(t):
	r = requests.post(url, data={'dropDownTypeVoie': t})
	soup = BeautifulSoup(r.text, 'html.parser')
	return ["{ct}|{s}".format(ct=t, s=o.get_text()) for o in soup.find_all('option')[1:]]

def get_numbers(s):
	r = requests.post(url, data={'dropDownNomVoie': s})
	soup = BeautifulSoup(r.text, 'html.parser')
	return ["{cts}|{n}".format(cts=s, n=o.get_text()) for o in soup.find_all('option')[1:]]

def get_res(n):
	r = requests.post(url, data={'dropDownNumVoie': n})
	soup = BeautifulSoup(r.text, 'html.parser')
	return [(n, soup.get_text()[12:])]

r = requests.post(url, data={'dropdownValueCommune': 'VOREPPE'})

soup = BeautifulSoup(r.text, 'html.parser')

types = ["{c}|{t}".format(c=city, t=o.get_text()) for o in soup.find_all('option')[2:]]

streets = reduce((lambda x, y: x + y), [get_streets(t) for t in types])
numbers = reduce((lambda x, y: x + y), [get_numbers(s) for s in streets])
res = reduce((lambda x, y: x + y), [get_res(n) for n in numbers])

for r in res:
	print(";".join(r))